# si-services
Elixir Norway Shared Infrastructure  - service onboarding

# How to use it?
Run ```ansible-galaxy role install -r requirements.yaml``` to install the role.

After this, you can run the playbook using ```ansible-playbook site-telegraf-install.yaml``` to install telegraf.

Remember to configure the variables and also the host vars in host.inv.
